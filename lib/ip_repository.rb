# frozen_string_literal: true

# Repository to store ips and store the webpages they have visited
class IPRepository
  attr_reader :ips

  def initialize
    @ips = {}
  end

  # Returns either an existing webpage hash, or creates a new hash
  def find_or_create(ip)
    @ips[ip] || @ips[ip] = {}
  end
end
