# frozen_string_literal: true

# Decorates the output of an array of webpages with hashes of their visits
class Printer
  def self.most_of(webpages, type)
    webpages.sort_by { |_k, v| v[type] }.reverse.map do |path, visit_hash|
      puts "#{path} has #{visit_hash[type]} #{type.to_s.split('_').join(' ')}"
    end
  end
end
