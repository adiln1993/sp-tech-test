# frozen_string_literal: true

require_relative '../lib/webpage_repository'

RSpec.describe WebpageRepository do
  describe '#find_or_create' do
    let(:webpage_repository) { described_class.new }
    context 'when it is a new webpage' do
      before do
        webpage_repository.find_or_create('/about')
      end

      it 'adds a hash to the webpages variable' do
        expect(webpage_repository.webpages['/about']).not_to be nil
      end

      it 'returns a hash with keys unique_visits and visits, with both values set to 0' do
        expect(webpage_repository.webpages['/about']).to eq({ unique_visits: 0, visits: 0 })
      end
    end

    context 'when it is an existing webpage' do
      before do
        wp = webpage_repository.find_or_create('/about')
        wp[:unique_visits] += 2
        wp[:visits] += 2
      end

      it 'returns a hash with the existing values' do
        expect(webpage_repository.webpages['/about']).to eq({ unique_visits: 2, visits: 2 })
      end
    end
  end
end
