# frozen_string_literal: true

# Repository to store webpages and increment their unique and regular visits
class WebpageRepository
  attr_reader :webpages

  def initialize
    @webpages = {}
  end

  # Returns either an existing webpage hash, or creates a new hash
  def find_or_create(webpage)
    @webpages[webpage] || @webpages[webpage] = { unique_visits: 0, visits: 0 }
  end
end
