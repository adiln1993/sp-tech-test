# frozen_string_literal: true

require_relative '../lib/ip_repository'

RSpec.describe IPRepository do
  describe '#find_or_create' do
    let(:ip_repository) { described_class.new }
    context 'when it is a new ip' do
      before do
        ip_repository.find_or_create('127.0.0.1')
      end

      it 'adds an empty hash with the ip as key to ips ivariable' do
        expect(ip_repository.ips['127.0.0.1']).to eq({})
      end
    end

    context 'when it is a ip that has visited /about' do
      before do
        ip = ip_repository.find_or_create('127.0.0.1')
        ip['/about'] = true
      end

      it 'returns a hash with "/about" as key and true as value' do
        expect(ip_repository.ips['127.0.0.1']).to eq({ '/about' => true })
      end
    end
  end
end
