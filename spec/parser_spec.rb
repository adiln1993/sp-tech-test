require_relative '../lib/parser'

RSpec.describe Parser do
  let(:subject) { described_class.new }
  let(:test_file) { File.open(File.join(Dir.pwd, '/spec/fixtures/webserver_test.log')) }

  describe '#run' do
    it 'returns a hash, with keys being webpages and values being a hash of visits' do
      expect(subject.run(test_file)).to eq(
        {
          '/about/2' => { unique_visits: 1, visits: 1 },
          '/contact' => { unique_visits: 1, visits: 1 },
          '/help_page/1' => { unique_visits: 3, visits: 3 },
          '/home' => { unique_visits: 1, visits: 1 },
          '/index' => { unique_visits: 1, visits: 1 }
        }
      )
    end
  end
end
