# frozen_string_literal: true

require_relative './lib/printer'
require_relative './lib/parser'

parsed_log = Parser.new.run(ARGV[0])

Printer.most_of(parsed_log, :visits)
puts "\n\n"
Printer.most_of(parsed_log, :unique_visits)
