require_relative './ip_repository'
require_relative './webpage_repository'

# Parses each line of the log file and counts unique and non unique visits
class Parser
  def initialize
    @webpage_repo = WebpageRepository.new
    @ip_repo = IPRepository.new
  end

  def run(log)
    File.foreach(log) do |line|
      path, ip_address = line.split(' ')
      webpage, ip = find_webpage_and_ip(path, ip_address)
      webpage[:visits] += 1
      unless ip[path]
        webpage[:unique_visits] += 1
        ip[path] = true
      end
    end
    @webpage_repo.webpages
  end

  private

  def find_webpage_and_ip(path, ip_address)
    [@webpage_repo.find_or_create(path), @ip_repo.find_or_create(ip_address)]
  end
end
