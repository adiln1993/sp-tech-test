# Server log parser

## Pre-requitsites

- Ruby 3.1.2
- Bundler 2.3.7

## Approach

I took what may seem like an odd approach here, by favouring Hash look ups over enumerating arrays for both counting and checking if a visit was unique. 

I did this for mainly to increase efficiency: the time complexity of looking up hash keys is O(1), versus the potential O(n*2) that could come about from looping over arrays. At 500 lines, this difference could be neglible, but since this is a webserver log, I thought it was a fair assumption to make that the implementation should be scalable. So the time complexity for the program is O(N), where N is the number of lines in the webserver.log file.

I also used `File.foreach` for a similar reason; for this program to be potentially used with large log files, reading and parsing each line rather then loading the entire file into memory seemed ideal.

I spefically avoided creating objects for `Webpage` and `IP` for similar reasons; neither object would have any complex behaviour, and the program is more efficient since it does not have to allocate multiple objects. 

I quite liked the repository approach as it could be used with adapters to potentially read/write to databases, or other APIs. 



## Potential Improvements

I would likely create a base repository class that the `WebpageRepository` and `IPRepository` could inherit from; the instance variable could be renamed to `@records` and the `find_or_create` method could be refactored to reduce duplication. 

I would also probably use [Thor](http://whatisthor.com) to handle inputs and to document the usage of the application a bit better, as well as to manage bad inputs. Speaking of, I didn't manage much in exception handling of the type of file or the contents of the file.

I would probably simplify the `lib/parser.rb` file as well; there is a lot going on there which could be split up into smaller, more modular pieces. 

## Final Thoughts

Great fun with this challenge! I like that it's open ended but fairly simple. 

